/**
  @file SSD1331.h
*/

#ifndef SSD1331_h
#define SSD1331_h

#include "Arduino.h"
#include "Adafruit_GFX.h"
#include "Adafruit_SPITFT.h"
#include <SPI.h>

/*********  Display Configuration  **********/

/*--------  Color  --------*/

// Color definitions
#define BLACK 0x0000
#define BLUE 0x001F
#define RED 0x8800
#define GREEN 0x3c60
#define CYAN 0x07FF
#define PURPLE 0x987B
#define YELLOW 0xFFE0
#define WHITE 0xFFFF
#define GREY 0xDEFB
#define NGREEN 0x5c0

/*!
    * Different implementations of the SSD1331 controller to display modules
    * may use either RGB or BGR color ordering. The NKK SmartSwitch uses BGR.
    * Keeping the option for future potential hardware changes.
    * In the setup commands, these colors are referred to by order:
    * A (blue)  |  B (green)  |  C (red)
    */

//#define SSD1331_COLORORDER_RGB
#define SSD1331_COLORORDER_BGR

#define SSD1331_DELAYS_HWFILL (20) // Timing Delays
#define SSD1331_DELAYS_HWLINE (10) //

/*--------  Fundamental Commands  --------*/

#define SSD1331_SetColumn 0x15      // column addressing
#define SSD1331_SetRow 0x75         // row addressing
#define SSD1331_CONTRASTA 0x81      // set (blue) contrast
#define SSD1331_CONTRASTB 0x82      // set (green) contrast
#define SSD1331_CONTRASTC 0x83      // set (red) contrast
#define SSD1331_MASTERCURRENT 0x87  // master current attenuation factor (0-15)
#define SSD1331_PRECHARGEA 0x8A     // set (blue) precharge speed
#define SSD1331_PRECHARGEB 0x8B     // set (green) precharge speed
#define SSD1331_PRECHARGEC 0x8C     // set (red) precharge speed
#define SSD1331_SETREMAP 0xA0       // set the remap ordering
#define SSD1331_STARTLINE 0xA1      // set start line register by row (always 0)
#define SSD1331_DISPLAYOFFSET 0xA2  // set vertical offset by com (always 0)
#define SSD1331_NORMALDISPLAY 0xA4  // display normal
#define SSD1331_DISPLAYALLON 0xA5   // all pixels on at GS63
#define SSD1331_DISPLAYALLOFF 0xA6  // all pixels off
#define SSD1331_INVERTDISPLAY 0xA7  // inverse display
#define SSD1331_SETMULTIPLEX 0xA8   // set mux ratio to N+1 mux
#define SSD1331_SETMASTER 0xAD      // set master config. ```P30```
#define SSD1331_DISPLAYOFF 0xAE     // naptime (aka sleep mode)
#define SSD1331_DISPLAYON 0xAF      // display on, standard brightness
#define SSD1331_POWERMODE 0xB0      // use less juice (aka power save)
#define SSD1331_PRECHARGE 0xB1      // phase 1, phase 2 period
#define SSD1331_CLOCKDIV 0xB3       // divide the display clock freq
#define SSD1331_PRECHARGELEVEL 0xBB // set precharge voltage for all three colors. ```P31```
#define SSD1331_VCOMH 0xBE          // set COM deselect voltage (Vcomh)

/*! @brief these commands are either unused, or untested
 * 
 * #define SSD1331_DISPLAYDIM 0xAC // display on, dimmed mode.
 * #define SSD1331_GRAYSCALE 0xB8 // set grayscale table. ```P31```
 * #define SSD1331_CMDLOCK 0xFD // Lock SSD1331 from accepting commands from MCU ```P31```
 */

/*--------  Graphic Acceleration Commands  --------*/

#define SSD1331_DRAWLINE 0x21 // Draw the line!
#define SSD1331_DRAWRECT 0x22 // Draw the rectangle!
#define SSD1331_FILL 0x26     // Enable / disable fill the rectangle!

/*! @brief these commands are either unused, or untested
 * #define SSD1331_COPY 0x23 // copy a region of the display ```P32```
 * #define SSD1331_DIMWINDOW 0x24 // dim a region of the display ```P32```
 * #define SSD1331_CLEARWINDOW 0x25 // clear a region of the display ```P32```
 * #define SSD1331_SCROLLSETUP 0x27 // define scrolling behavior ```P33```
 * #define SSD1331_SCROLLDISABLE 0x2E //deactivate scrolling action
 * #define SSD1331_SCROLLENABLE 0x2F // activate scrolling defined by _SCROLLSETUP
 */

/********* Classes to interface with SPITFT **********/

class SmartSwitch : public Adafruit_SPITFT
{
public:
  // Software SPI (bit-bang)
  SmartSwitch(int8_t cs, int8_t dc, int8_t mosi, int8_t sclk, int8_t rst);

  void begin(uint32_t begin = 6000000);
  void SetAddrWindow(uint16_t x, uint16_t y, uint16_t w, uint16_t h);
  void EnableDisplay(boolean enable);

  static const int16_t TFTWIDTH = 96;
  static const int16_t TFTHEIGHT = 64;

private:
};

#endif
