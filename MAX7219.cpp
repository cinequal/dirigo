/**
	cqscout peripheral library - MAX7219

	Use:	The MAX7219 LED matrix display IC interfaces with the MCU over SPI
				bus to drive a series of 7-segment displays (4 per IC in our case)

	Datasheet  : https://datasheets.maximintegrated.com/en/ds/MAX7219-MAX7221.pdf

	cribbed from wayoda on github

	@file MAX7219.cpp
	@author tyler van drell // cinequal
	@version 2.1 20-Aug-2020
	@copyright MIT license
	@headerfile <MAX7219.h>
*/

#include "signals.h"
#include "MAX7219.h"


MAX7219::MAX7219(int numDevices) {
    if(numDevices<=0 || numDevices>8 )
        numDevices=8;
    maxDevices=numDevices;
    pinMode(MAX7219_DATA,OUTPUT);
    pinMode(MAX7219_SCLK,OUTPUT);
    pinMode(MAX7219_LOAD,OUTPUT);
    digitalWrite(MAX7219_LOAD,HIGH);
    for(int i=0;i<64;i++) 
        status[i]=0x00;
    for(int i=0;i<maxDevices;i++) {
        spiTransfer(i,MAX7219_TEST,0);
        //scanlimit is set to max on startup
        SetScanLimit(i,7);
        //decode is done in source
        spiTransfer(i,MAX7219_DECODE,0);
        ClearDisplay(i);
        //we go into Shutdown-mode on startup
        Shutdown(i,true);
    }
}

int MAX7219::GetDeviceCount() {
    return maxDevices;
}

void MAX7219::Shutdown(int addr, bool b) {
    if(addr<0 || addr>=maxDevices)
        return;
    if(b)
        spiTransfer(addr, MAX7219_Shutdown,0);
    else
        spiTransfer(addr, MAX7219_Shutdown,1);
}

void MAX7219::SetScanLimit(int addr, int limit) {
    if(addr<0 || addr>=maxDevices)
        return;
    if(limit>=0 && limit<8)
        spiTransfer(addr, MAX7219_SCANLIMIT,limit);
}

void MAX7219::SetIntensity(int addr, int intensity) {
    if(addr<0 || addr>=maxDevices)
        return;
    if(intensity>=0 && intensity<16)	
        spiTransfer(addr, MAX7219_INTENSITY,intensity);
}

void MAX7219::ClearDisplay(int addr) {
    int offset;

    if(addr<0 || addr>=maxDevices)
        return;
    offset=addr*8;
    for(int i=0;i<8;i++) {
        status[offset+i]=0;
        spiTransfer(addr, i+1,status[offset+i]);
    }
}

void MAX7219::SetLED(int addr, int row, int column, boolean state) {
    int offset;
    byte val=0x00;

    if(addr<0 || addr>=maxDevices)
        return;
    if(row<0 || row>7 || column<0 || column>7)
        return;
    offset=addr*8;
    val=B10000000 >> column;
    if(state)
        status[offset+row]=status[offset+row]|val;
    else {
        val=~val;
        status[offset+row]=status[offset+row]&val;
    }
    spiTransfer(addr, row+1,status[offset+row]);
}

void MAX7219::SetRow(int addr, int row, byte value) {
    int offset;
    if(addr<0 || addr>=maxDevices)
        return;
    if(row<0 || row>7)
        return;
    offset=addr*8;
    status[offset+row]=value;
    spiTransfer(addr, row+1,status[offset+row]);
}

void MAX7219::SetColumn(int addr, int col, byte value) {
    byte val;

    if(addr<0 || addr>=maxDevices)
        return;
    if(col<0 || col>7) 
        return;
    for(int row=0;row<8;row++) {
        val=value >> (7-row);
        val=val & 0x01;
        SetLED(addr,row,col,val);
    }
}

void MAX7219::SetDigit(int addr, int digit, byte value, boolean dp) {
    int offset;
    byte v;

    if(addr<0 || addr>=maxDevices)
        return;
    if(digit<0 || digit>7 || value>15)
        return;
    offset=addr*8;
    v=pgm_read_byte_near(charTable + value); 
    if(dp)
        v|=B10000000;
    status[offset+digit]=v;
    spiTransfer(addr, digit+1,v);
}

void MAX7219::SetChar(int addr, int digit, char value, boolean dp) {
    int offset;
    byte index,v;

    if(addr<0 || addr>=maxDevices)
        return;
    if(digit<0 || digit>7)
        return;
    offset=addr*8;
    index=(byte)value;
    if(index >127) {
        //no defined beyond index 127, so we use the space char
        index=32;
    }
    v=pgm_read_byte_near(charTable + index); 
    if(dp)
        v|=B10000000;
    status[offset+digit]=v;
    spiTransfer(addr, digit+1,v);
}

void MAX7219::spiTransfer(int addr, volatile byte opcode, volatile byte data) {
    //Create an array with the data to shift out
    int offset=addr*2;
    int maxbytes=maxDevices*2;

    for(int i=0;i<maxbytes;i++)
        spidata[i]=(byte)0;
    //put our device data into the array
    spidata[offset+1]=opcode;
    spidata[offset]=data;
    //enable the line 
    digitalWrite(MAX7219_LOAD,LOW);
    //Now shift out the data 
    for(int i=maxbytes;i>0;i--)
        shiftOut(MAX7219_DATA,MAX7219_SCLK,MSBFIRST,spidata[i-1]);
    //latch the data onto the display
    digitalWrite(MAX7219_LOAD,HIGH);
}    
