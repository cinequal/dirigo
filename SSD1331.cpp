/**
	cqscout peripheral library - SSD1331
	    
	Use:  The SSD1331 OLED display matrix driver interfaces the raw display in 
        the NKK OLED SmartSwitch with the MCU over an SPI interface
	
	Datasheet: https://www.newhavendisplay.com/resources_dataFiles/datasheets/OLEDs/SSD1331.pdf
	
	@file SSD1331.cpp
	@author tyler van drell // cinequal
	@version 1.3 20-Aug-2020
	@copyright MIT license
	@headerfile <SSD1331.h>
*/

#include "SSD1331.h"

//#include "pins_arduino.h"
//#include "wiring_private.h"

/***********************************/

void SmartSwitch::SetAddrWindow(uint16_t x, uint16_t y, uint16_t w, uint16_t h) {

  uint8_t x1 = x;
  uint8_t y1 = y;
  if (x1 > 95)
    x1 = 95;
  if (y1 > 63)
    y1 = 63;

  uint8_t x2 = (x + w - 1);
  uint8_t y2 = (y + h - 1);
  if (x2 > 95)
    x2 = 95;
  if (y2 > 63)
    y2 = 63;

  if (x1 > x2)
  {
    uint8_t t = x2;
    x2 = x1;
    x1 = t;
  }
  if (y1 > y2)
  {
    uint8_t t = y2;
    y2 = y1;
    y1 = t;
  }

  sendCommand(0x15); // Column addr set
  sendCommand(x1);
  sendCommand(x2);

  sendCommand(0x75); // Column addr set
  sendCommand(y1);
  sendCommand(y2);

  startWrite();
}

/*--------  Initialization setup & op params setting  --------*/
//  freq  Desired SPI clock frequency

void SmartSwitch::begin(uint32_t freq)
{
  initSPI(freq);
  // Initialization Sequence
  sendCommand(SSD1331_DISPLAYOFF);
  sendCommand(SSD1331_SETREMAP);
  sendCommand(0x6C); // BGR Color
  sendCommand(SSD1331_STARTLINE);
  sendCommand(0x0);
  sendCommand(SSD1331_DISPLAYOFFSET);
  sendCommand(0x0);
  sendCommand(SSD1331_NORMALDISPLAY);
  sendCommand(SSD1331_SETMULTIPLEX);
  sendCommand(0x3F); // 0x3F 1/64 duty
  sendCommand(SSD1331_SETMASTER);
  sendCommand(0x8E);
  sendCommand(SSD1331_POWERMODE);
  sendCommand(0x0B);
  sendCommand(SSD1331_PRECHARGE);
  sendCommand(0x31);
  sendCommand(SSD1331_CLOCKDIV);
  sendCommand(0xF0); // 7:4 = Osc Freq, 3:0 = CLK Div Ratio (A[3:0]+1 = 1..16)
  sendCommand(SSD1331_PRECHARGEA);
  sendCommand(0x64);
  sendCommand(SSD1331_PRECHARGEB);
  sendCommand(0x78);
  sendCommand(SSD1331_PRECHARGEC);
  sendCommand(0x64);
  sendCommand(SSD1331_PRECHARGELEVEL);
  sendCommand(0x3A);
  sendCommand(SSD1331_VCOMH);
  sendCommand(0x3E);
  sendCommand(SSD1331_MASTERCURRENT);
  sendCommand(0x06);
  sendCommand(SSD1331_CONTRASTA);
  sendCommand(0x91);
  sendCommand(SSD1331_CONTRASTB);
  sendCommand(0x50);
  sendCommand(SSD1331_CONTRASTC);
  sendCommand(0x7D);
  sendCommand(SSD1331_DISPLAYON);
  _width = TFTWIDTH;
  _height = TFTHEIGHT;
}

SmartSwitch::SmartSwitch(int8_t cs, int8_t dc, int8_t mosi, int8_t sclk, int8_t rst)
    : Adafruit_SPITFT(TFTWIDTH, TFTHEIGHT, cs, dc, mosi, sclk, rst, -1) {}