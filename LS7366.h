/**
  @file LS7366.h
*/

#ifndef LS7366_h
#define LS7366_h

/*********  MDR0 Configuration  **********/

/*--------  B1 B0 (Quadrature Mode)  --------*/
#define QUADRX0 0x00 // non-quadrature mode
#define QUADRX1 0x01 // X1 quadrature mode
#define QUADRX2 0x02 // X2 quadrature mode
#define QUADRX4 0x03 // X4 quadrature mode

/*--------  B3 B2 (Counting Mode)  --------*/
#define COUNT_FREE 0x00   // continuous count mode
#define COUNT_SINGLE 0x04 // disable on CY, BW; enable on RST, LOAD
#define COUNT_RLIMIT 0x08 // disable count when outside range (0, DTR)
#define COUNT_MODULO 0x0C // divide input count clock frequency by (DTR + 1)

/*--------  B5 B4 (Index Action)  --------*/
#define INDX_DIS 0x00   // no action
#define INDX_LOADC 0x10 // xfer DTR to CNTR
#define INDX_RSTC 0x20  // set CNTR = 0
#define INDX_LOADO 0x30 // xfer CNTR to OTR

/*--------  B6 (Index Sampling)  --------*/
#define INDX_ASYNC 0x00 // asynchronous index
#define INDX_SYNCH 0x40 // synchronous index

/*--------  B7 (Clock Filter  --------*/
#define FILTERX1 0x00 // filter sck freq division factor 1
#define FILTERX2 0x80 // filter sck freq division factor 1

/*********  MDR1 Configuration  **********/
/*--------  B1 B0 (Counter Mode)  --------*/
#define BYTE_4 0x00 // four byte mode
#define BYTE_3 0x01 // three byte mode
#define BYTE_2 0x02 // two byte mode
#define BYTE_1 0x03 // one byte mode

/*--------  B2 (Counter State)  --------*/
#define CNTR_EN 0x00  // counting enabled
#define CNTR_DIS 0x04 // counting disabled

/*--------  B3 (Unused)  --------*/

// nothing here

/*--------  B7 B6 B5 B4 (Flag Action)  --------*/
#define FLAG_DISABLE 0x00 //all flags disabled
#define FLAG_IDX 0x10     //B4 - Flag on index
#define FLAG_CMP 0x20     //B5 - Flag on compare operation
#define FLAG_BW 0x40      //B6 - Flag on borrow operation
#define FLAG_CY 0x80      //B7 - Flag on carry operation

/*********  op codes  **********/
#define CLR_MDR0 0x08
#define CLR_MDR1 0x10
#define CLR_CNTR 0x20
#define CLR_STR 0x30
#define READ_MDR0 0x48
#define READ_MDR1 0x50
#define READ_CNTR 0x60
#define READ_OTR 0x68
#define READ_STR 0x70
#define WRITE_MDR1 0x90
#define WRITE_MDR0 0x88
#define WRITE_DTR 0x98
#define LOAD_CNTR 0xE0
#define LOAD_OTR 0xE4

class LS7366
{
public:
  LS7366(byte cs_line);
  void clear_mdr0();
  void clear_mdr1();
  void clear_counter();
  void clear_str();
  byte read_mdr0();
  byte read_mdr1();
  unsigned long read_counter();
  unsigned long read_otr();
  byte read_str();
  void write_mdr0(byte val);
  void write_mdr1(byte val);
  void write_dtr(unsigned long val);
  void load_counter();
  void load_otr();

private:
  byte cs_line;
  byte datawidth;
  long LeftExtendMSB(long val);
};

#endif
