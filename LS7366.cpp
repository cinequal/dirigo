/**
	cqscout peripheral library - LS7366
	
  Use:	The LS7366R quadarature counter ASIC forms the backbone of linear speed
				and motion tracking for the scout &co. film and tape transfer mechanism

	Datasheet: https://lsicsi.com/datasheets/LS7366R.pdf

	@file LS7366.cpp
  @author tyler van drell // cinequal
  @version 1.12 20-Aug-2020
  @copyright MIT license
  @headerfile <LS7366.h>
*/

#include "Arduino.h"
#include "LS7366.h"
#include "SPI.h"

/*
  Returns 32bit _unsigned_ long count values. If you want to use _long_ values,
	- Change all declarations from (unsigned long) to (long)
	- Use the left-extend_MSB() helper function when you call read_counter() and read_otr()
  
  This just saves you from doing the sign extension (not that kind of sexting) using
  bitwise operators, which is a quick pathway to inconsistency.
*/

LS7366::LS7366(byte chip_select)
{
	cs_line = chip_select;
	datawidth = 4; // default value

	pinMode(cs_line, OUTPUT);
	digitalWrite(cs_line, HIGH); // set phasers to fun (AKA csel to high)
	SPI.begin();
}

void LS7366::clear_mdr0()
{
	digitalWrite(cs_line, LOW);
	SPI.transfer(CLR_MDR0);
	digitalWrite(cs_line, HIGH);
}

void LS7366::clear_mdr1()
{
	digitalWrite(cs_line, LOW);
	SPI.transfer(CLR_MDR1);
	digitalWrite(cs_line, HIGH);
}

void LS7366::clear_counter()
{
	digitalWrite(cs_line, LOW);
	SPI.transfer(CLR_CNTR);
	digitalWrite(cs_line, HIGH);
}

void LS7366::clear_str()
{
	digitalWrite(cs_line, LOW);
	SPI.transfer(CLR_STR);
	digitalWrite(cs_line, HIGH);
}

byte LS7366::read_mdr0()
{
	digitalWrite(cs_line, LOW);
	SPI.transfer(READ_MDR0);
	byte readout = SPI.transfer(0x00);
	digitalWrite(cs_line, HIGH);
	return readout;
}

byte LS7366::read_mdr1()
{
	digitalWrite(cs_line, LOW);
	SPI.transfer(READ_MDR1);
	byte readout = SPI.transfer(0x00);
	digitalWrite(cs_line, HIGH);
	return readout;
}

unsigned long LS7366::read_counter()
{
	unsigned long readout = 0;
	byte bytes_to_read = datawidth;
	byte val;

	digitalWrite(cs_line, LOW);
	SPI.transfer(READ_CNTR);
	while (bytes_to_read > 0)
	{
		val = SPI.transfer(0x00);
		bytes_to_read--;
		readout = (readout << 8) | val;
	}
	digitalWrite(cs_line, HIGH);
	return readout;
}

unsigned long LS7366::read_otr()
{
	unsigned long readout = 0;
	byte bytes_to_read = datawidth;
	byte val;

	digitalWrite(cs_line, LOW);
	SPI.transfer(READ_OTR);

	while (bytes_to_read > 0)
	{
		val = SPI.transfer(0x00);
		bytes_to_read--;
		readout = (readout << 8) | val;
	}
	digitalWrite(cs_line, HIGH);
	return readout;
}

byte LS7366::read_str()
{
	digitalWrite(cs_line, LOW);
	SPI.transfer(READ_STR);
	byte readout = SPI.transfer(0x00);
	digitalWrite(cs_line, HIGH);
	return readout;
}

void LS7366::write_mdr0(byte val)
{
	digitalWrite(cs_line, LOW);
	SPI.transfer(WRITE_MDR0);
	SPI.transfer(val);
	digitalWrite(cs_line, HIGH);
}

void LS7366::write_mdr1(byte val)
{
	digitalWrite(cs_line, LOW);
	SPI.transfer(WRITE_MDR1);
	SPI.transfer(val);
	digitalWrite(cs_line, HIGH);
	//set data width pro
	datawidth = 0x04 - (0x03 & val);
}

void LS7366::write_dtr(unsigned long val)
{
	unsigned long value_to_write = val;
	byte i = 0;

	digitalWrite(cs_line, LOW);
	SPI.transfer(WRITE_DTR);
	for (i = 0; i < datawidth; i++)
	{
		value_to_write = (byte)(val >> (8 * (datawidth - 1 - i)));
		SPI.transfer(value_to_write);
	}
	digitalWrite(cs_line, HIGH);
}

void LS7366::load_counter()
{
	digitalWrite(cs_line, LOW);
	SPI.transfer(LOAD_CNTR);
	digitalWrite(cs_line, HIGH);
}

void LS7366::load_otr()
{
	digitalWrite(cs_line, LOW);
	SPI.transfer(LOAD_OTR);
	digitalWrite(cs_line, HIGH);
}

long LS7366::LeftExtendMSB(long val)
{
	long value_to_return;
	long MSB;
	if (datawidth == 4)
	{
		value_to_return = val;
	}
	else
	{
		MSB = (val >> (datawidth * 8 - 1)) & 0x0001;
		if (MSB == 0) // "You cannot extend what does not exist" - Confucius, probably
		{
			value_to_return = val;
		}
		else
		{
			switch (datawidth)
			{
			case 1:
				value_to_return = 0xFFFFFF00 | val;
				break;
			case 2:
				value_to_return = 0xFFFF0000 | val;
				break;
			case 3:
				value_to_return = 0xFF000000 | val;
				break;
			default:
				// twiddle ya thumbs
				break;
			}
		}
	}

	return value_to_return;
}
