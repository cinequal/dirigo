/*

  VERSION 2.0 BECAUSE I SAY SO

  dīrigō (from the Latin  `dis-` + `regō`) tr. "I govern (rule)"

  Dirigo is the top-level firmware for the MCU in the scout,
  governing all activities.

  Also, dirigo is the motto of my home, the great state of Maine.

*/

#include <Arduino.h>
#include <SPI.h>
#include "SSD1331.h"
#include "LS7366.h"
#include "PID.h"
#include "Adafruit_GFX.h"
#include "signals.h"
#include "MAX7219.h"

// SmartSwitch switch_left = SmartSwitch(SS_1, S_DC, S_MOSI, S_SCK, S_RES);
// SmartSwitch switch_center = SmartSwitch(SS_2, S_DC, S_MOSI, S_SCK, S_RES);
// SmartSwitch switch_right = SmartSwitch(SS_3, S_DC, S_MOSI, S_SCK, S_RES);
// MAX7219 timecode = MAX7219(2);

LS7366 tachometer(TACH_SS);
MAX7219 ticker = MAX7219(2);

int frameInterval = 2575; // See notes below on ::write_dtr()
int feet = 0;
volatile int flags = 0;

int userInput = 0;
int framePulses = 0;
int lightLevel = 0;
int userLux = 0;

bool fanState1 = false;
bool fanState2 = false;
bool ledState = false;
bool transportState = false;

float oldLinearFeet = 0;
float newLinearFeet = 0; // feet of film measurement

long baselineCount = 0;
long updatedCount = 0;

unsigned long previousMillis = 0;    // millis() returns unsigned longs
unsigned long sampleInterval = 200; // delay time in millis
volatile int previousFrames = 0;

// just pid stuff
double speedSetPoint, SpeedInput, SpeedOutput;
double SpeedValue = 0;
double Kp1 = 0.6, Ki1 = 1.5, Kd1 = 0; // ki was 0.5
PID myPID1(&SpeedInput, &SpeedOutput, &speedSetPoint, Kp1, Ki1, Kd1, DIRECT);

volatile int targetSpeed = 10;
float linearSpeed = 0; // film linear speed in fps

void setup()
{
  InitiateIO();
  Serial.begin(115200);
  cheatsheet();
  digitalWrite(TACH_EN, HIGH);
  pinMode(CAM_TRIG, INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(CAM_TRIG), printFrameCount, RISING);
    tachometer.write_mdr0(FILTERX1 | INDX_DIS | COUNT_MODULO | QUADRX4);
  tachometer.write_mdr1(FLAG_CMP | CNTR_EN | BYTE_4);
  tachometer.clear_counter();
  tachometer.clear_str();
  tachometer.write_dtr(2575);

    speedSetPoint = 10;

  //turn the PID on
  myPID1.SetMode(AUTOMATIC);
  myPID1.SetOutputLimits(128, 180);
}

// void increment()
// {
//   flags++;
// }

void loop()
{
  mainRoutine();
  if (transportState == true)
  {
    Serial.println(framePulses);
    myPID1.Compute();
    Transport();
  }
  else
  {
    digitalWrite(M1_EN, LOW);
    digitalWrite(M2_EN, LOW);
  }
  //	feet = flags / 40;
  //	Serial.println(feet);
  //	delay(100);
}

void InitiateIO()
{

  /*--------  Core Startup IO --------*/

  pinMode(BUCK12_EN, OUTPUT); // turn on 12V dc/dc converter
  digitalWrite(BUCK12_EN, HIGH);

  pinMode(FAN1_PWM, OUTPUT);
  pinMode(FAN2_PWM, OUTPUT);
  pinMode(LED_PWM, OUTPUT);

  /*--------  Peripheral misc. IO --------*/
  /* !!! NOTE TO FUTURE SELF !!!

  	 Hows aboot you stop being a schmuck, and put this *misc* IO pins in their
  	 own functions in the driver libraries for each one?				 CRAZY, RIGHT?

  */

  pinMode(TACH_EN, OUTPUT);

  /*--------  Motion Inputs --------*/

  pinMode(CAM_TRIG, INPUT);
  pinMode(M1_HLFB, INPUT);
  pinMode(M2_HLFB, INPUT);
  pinMode(MOTOR_DETECT, INPUT);

  /*--------  Motion Outputs --------*/

  pinMode(M1_EN, OUTPUT);  // logic high to enable motors
  pinMode(M2_EN, OUTPUT);  // * technically you don't need to set a pinMode() for pins
  pinMode(M1_INA, OUTPUT); //   being used for analogOutput(), but it's a good idea
  pinMode(M1_INB, OUTPUT); //   to do so as a matter of course to remind yourself
  pinMode(M2_INA, OUTPUT); // * what things are doing and to keep the code clean.
  pinMode(M2_INB, OUTPUT);

  /*
    REGARDING THE TORQUE.... aka read me

  	When you set the standard and alternate torque limits in MSP (5% and 50%, for example),
  	the behavior is as follows -
  		- no PWM means the motor will max out at 5% torque. You can move them manually.
  		- PWM range from ~3 to ~252 will be a linear combination of the two limits.
  		- The signal limits the force, not the direction, of the applied torque. It will
    		apply equally whether moving forward or backward.
  		- 0 is treated as no input - so the motors will be back to the standard limit,
    		allowing manual movement.
  */

  /*--------  user interface stuff --------*/
  pinMode(SW_1, INPUT_PULLUP);
  pinMode(SW_2, INPUT_PULLUP);
  pinMode(SW_3, INPUT_PULLUP);
  pinMode(ESTOP_OK, OUTPUT);
  pinMode(ESTOP_ALERT, OUTPUT);
}

// void UpdateVelocity() {}
// void UpdateTimecode() {}
// void UpdateUI() {}
// void TransportHold() {}
// void TransportRelease() {}

void Transport()
{
  unsigned long currentMillis = millis(); // what time is it
  if ((currentMillis - previousMillis) >= sampleInterval)
  {
    previousMillis = currentMillis;

    // calculate linear velocity from frame count
    updatedCount = framePulses;
    linearSpeed = ((updatedCount - baselineCount)); // speed in mm/s

    newLinearFeet = newLinearFeet + oldLinearFeet;
    baselineCount = updatedCount;
    SpeedInput = linearSpeed;

    myPID1.Compute();
    // enable motors
    
    analogWrite(M1_INA, 10);
    analogWrite(M2_INA, 220);
    analogWrite(M1_INB, 128);
    analogWrite(M2_INB, SpeedOutput);
    //Serial.println(SpeedOutput);
    Serial.println(framePulses);
  }
}
// void TransportTest()
// {
//   Serial.println("TEST BEGIN");
//   delay(500);
//   Serial.println("TEST ACCEL");
//   // enable motors
//   digitalWrite(M1_EN, HIGH);
//   digitalWrite(M2_EN, HIGH);
//   analogWrite(M1_INA, 10);
//   analogWrite(M2_INA, 240);
//   analogWrite(M1_INB, 128);
//   analogWrite(M2_INB, 150);
//   Serial.println("TEST SPEED");
//   delay(10000);
//   Serial.println("TEST DECEL");
//   // drive both motors at a nominal speed clockwise. If max RPM = 300, 140 should yield ~24RPM forward
//   analogWrite(M2_INB, 134);
//   delay(500);
//   analogWrite(M2_INB, 128);
//   // remove applied torque and disable motors before starting over again
//   analogWrite(M1_INA, 0);
//   analogWrite(M2_INA, 0);
//   digitalWrite(M1_EN, LOW);
//   digitalWrite(M2_EN, LOW);
//   Serial.println("TEST DONE");
// }

// void TachometerTestInit()
// {

//   // Enable the LS7366 by setting the pin to high
//   pinMode(TACH_EN, OUTPUT);
//   digitalWrite(TACH_EN, HIGH);

//   /*
//     See LS7366.h for information about all the instruction bytes,
//     but these shouldn't really change. The key things to note are
//     that the COUNT_MODULO mode sets the IC to count a certain number
//     of quadrature pulses, and (almost) simultaneously perform an action
//     and clear the count. The action is a FLAG pulse, described below.
//   */

//   tachometer.write_mdr0(FILTERX1 | INDX_DIS | COUNT_MODULO | QUADRX4);

//   /*
//     The key item of note in this instruction word is the FLAG byte;
//     FLAG_CMP sets the IC to pulse the DFLAG line high. This signal is
//     sent to both the camera trigger, and pin 19 of the ATMEGA.

//     The long story of why we can use an ISR for counting is detailed at the
//     bottom of this file. Short story is, film slow processor fast; and when
//     the ISR halts the loop, PWM outputs don't drop out, and that's critical.

//     This won't work for bidirectional motion; that's the main limiting factor, but
//     moving backwards doesn't require (as) accurate of tracking.
//   */

//   tachometer.write_mdr1(FLAG_CMP | CNTR_EN | BYTE_4);
//   tachometer.clear_counter(); // clean up any extraneous garbage
//   tachometer.clear_str();     // status register only used for diagnostics really

//   /*
//     DTR (data register) stores the integer used in the COMPARE op of MDR1.

//     This requires tuning, to set the number of pulses that equal one 'frame'
//     worth of movement.
//   */
//   tachometer.write_dtr(frameInterval);

//   pinMode(CAM_TRIG, INPUT_PULLUP);
//   attachInterrupt(digitalPinToInterrupt(CAM_TRIG), incrementFrame, FALLING);

//   /*
//     Don't use operations directly in an attachInterrupt() ISR.
//     Use a separately defined function.
//   */

//   /*

//   	The pulse duration is highly consistent, so there is no need for a
//   	one-shot between the IC and the camera.
//   	using an attachInterrupt() rather than reading the count
//   	at a preset interval yields a more consistent result regarding frame counts.
//   	Since the ISR is triggered at the frame rate of the moving film, it doesn't
//   	interfere with other functions in the main loop.

//   	Reading the counter directly at a set interval could yield lost "counts" since
//   	the IC isn't going to return the count and simultaneously keep counting the
//   	quad pulses.

//   */
// }

// void TachometerTestLoop()
// {
//   feet = flags / 40; // once the var frameInterval is set, 40 frames = 1ft
//   Serial.println(feet);
//   delay(100); // I don't think delays prevent ISR's from executing.
//   // but if there's lag, you can shorten the delay or use a millis() timer
//   // to trigger the print instead.
// }

// void incrementFrame()
// {
//   flags++; // can i bring a plus one?
// }

// void FanLightTest()
// {

//   // residual code from the sketch previously called mppria.ino
//   /*
//     `Per manuum prōpriārum dīrēctū` - Directed by one's own hands.
//     Mppria (abbrev. `manu propria`) tr. 'by one's hand', to sign a letter.
//     ANYWAY, this is residual test code saved for reference.

//   */
//   analogWrite(FAN1_PWM, 5);
//   analogWrite(FAN2_PWM, 5);
//   analogWrite(LED_PWM, 250);
//   analogWrite(FAN1_PWM, 230);
//   analogWrite(FAN1_PWM, 5);
//   analogWrite(FAN2_PWM, 230);
//   analogWrite(FAN2_PWM, 5);
//   analogWrite(FAN1_PWM, 250);
//   analogWrite(FAN2_PWM, 250);
//   analogWrite(LED_PWM, 192);
//   analogWrite(LED_PWM, 128);
//   analogWrite(LED_PWM, 64);
//   delay(2000);
//   analogWrite(LED_PWM, 250);
//   analogWrite(FAN1_PWM, 5);
//   analogWrite(FAN2_PWM, 5);
//   delay(1000);
//   analogWrite(FAN1_PWM, 192);
//   analogWrite(FAN2_PWM, 192);
// }

// void triggerCamera()
// {
//   detachInterrupt(digitalPinToInterrupt(CAM_TRIG));
//   pinMode(CAM_TRIG, OUTPUT);
//   digitalWrite(CAM_TRIG, HIGH);
//   for (int i = 0; i <= 50; i++)
//   {
//     digitalWrite(CAM_TRIG, LOW);
//     delay(5);
//     digitalWrite(CAM_TRIG, HIGH);
//     delay(95);
//     Serial.print(".");
//   }
//   Serial.println("50 pulses sent. Done!");
//   pinMode(CAM_TRIG, INPUT);
// }

void printFrameCount()
{
  framePulses++;
  //Serial.print(" Fr:  ");
  Serial.println(framePulses);
  //Serial.print("\n");
}

void mainRoutine()
{

  if (Serial.available() > 0)
  {
    userInput = Serial.read();
    switch (userInput)
    {

    case 84:  // `T`
    case 116: //`t`
      transportState = !transportState;
      if (transportState == false)
      {
        Serial.println("MOTOR STOP");
        analogWrite(M2_INB, 140);
  delay(500);
  analogWrite(M2_INB, 128);
  // remove applied torque and disable motors before starting over again
  analogWrite(M1_INA, 0);
  analogWrite(M2_INA, 0);
  digitalWrite(M1_EN, LOW);
  digitalWrite(M2_EN, LOW);
        //digitalWrite(TACH_EN, LOW);
        //detachInterrupt(digitalPinToInterrupt(CAM_TRIG));
      }
      else if (transportState == true)
      {
        Serial.println("MOTOR START");
        digitalWrite(M1_EN, HIGH);
    digitalWrite(M2_EN, HIGH);
        //digitalWrite(TACH_EN, HIGH);
 
      }
      break;

    case 66: // `B`
    case 98: // `b`
      ledState = !ledState;
      if (ledState == false)
      {
        Serial.println("LED ON");
        analogWrite(LED_PWM, 20);
      }
      else
      {
        Serial.println("LED OFF");
        analogWrite(LED_PWM, 250);
      }
      break;

    case 69: // `E`
      Serial.println("Encoder Active. Spin to trigger Camera.");
      pinMode(CAM_TRIG, INPUT);
      digitalWrite(TACH_EN, HIGH);
      attachInterrupt(digitalPinToInterrupt(CAM_TRIG), printFrameCount, RISING);
      tachometer.write_mdr0(FILTERX1 | INDX_DIS | COUNT_MODULO | QUADRX4);
      tachometer.write_mdr1(FLAG_CMP | CNTR_EN | BYTE_4);
      tachometer.clear_counter();
      tachometer.clear_str();
      tachometer.write_dtr(2575);
      break;

    case 74:  // `J`
    case 106: // `j`
      //TransportTest();
      break;

      // case 101: // `e`
      //   Serial.println("Encoder Disabled.");
      //   detachInterrupt(digitalPinToInterrupt(CAM_TRIG));
      //   digitalWrite(TACH_EN, LOW);
      //   break;

      // case 80: // `P`
      //   Serial.println("Camera will trigger at 5Hz for a period of 10 seconds. Starts in 3sec.");
      //   delay(3000);
      //   triggerCamera();
      //   break;

    case 82:  // `R`
    case 114: // `r`
      fanState1 = !fanState1;
      if (fanState1 == false)
      {
        Serial.println("Fan 1 ON");
        analogWrite(FAN1_PWM, 250);
      }
      else
      {
        Serial.println("Fan 1 OFF");
        analogWrite(FAN1_PWM, 5);
      }
      break;

    case 83:  // `S`
    case 115: // `s`
      fanState2 = !fanState2;
      if (fanState2 == false)
      {
        Serial.println("Fan 2 ON");
        analogWrite(FAN2_PWM, 250);
      }
      else
      {
        Serial.println("Fan 2 OFF");
        analogWrite(FAN2_PWM, 5);
      }
      break;

    // other stuff
    case 63: // `?`
      cheatsheet();
      break;

      // case 33: // `!`
      //   Serial.println("Fans, Light, Camera Trigger all reset to inactive state.");
      //   analogWrite(FAN1_PWM, 0);
      //   analogWrite(FAN2_PWM, 0);
      //   analogWrite(LED_PWM, 250);
      //   break;
    }
    delay(1); // prevent garbage reads on tty
  }
}

void cheatsheet()
{
  Serial.println(" [ ? ] to show this menu.");
  Serial.println("keys not case sensitive");
  Serial.print('\n');
  Serial.println("LED Toggle            | B");
  Serial.println("Fan 1 Toggle          | R");
  Serial.println("Fan 2 Toggle          | S");
  Serial.println("Run Transport         | T");
//  Serial.println("Jog motors 5s         | J");
  Serial.println("-------------------------");
}

void TheBeastWithHowManyFeet()
{
  feet = flags / 40;
  Serial.println(feet);
  delay(100);
}
