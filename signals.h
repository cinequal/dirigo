/**
  @file signals.h
*/

#include <Wire.h>
#include <Arduino.h>

#ifndef signals_h
#define signals_h

/*********  POWER STUFF  **********/

#define BUCK12_EN 15 // PJ0

/*********  UNKNOWN  **********/
#define INH_EN 17			 // PH0
#define DISPLAY_RST 46 // PL3
#define DISPLAY_CS 45	 // PL4

/*********  SPI & IIC BUS  **********/

#define SPI_SCK 52	// PB1
#define SPI_MOSI 51 // PB2
#define SPI_MISO 50 // PB3
#define SDA 20			// PD1
#define SCL 21			// PD0

/*--------  SWITCHES  --------*/
#define S_SCK 28	//	SmartSwitch SWSPI Clock	(PA6)
#define S_DC 29		//	SmartSwitch SWSPI D/C	(PA7)
#define S_MOSI 31 //	SmartSwitch SWSPI Data	(PC6)
#define S_RES 30	//	SmartSwitch SWSPI Reset	(PC7)
#define SS_1 22		//	SmartSwitch_1 Chip Select	(PA0)
#define SS_2 23		//	SmartSwitch_2 Chip Select	(PA1)
#define SS_3 24		//	SmartSwitch_3 Chip Select	(PA2)
#define SW_1 25		//	SmartSwitch_1 Actuator	(PA3)
#define SW_2 26		//	SmartSwitch_2 Actuator	(PA4)
#define SW_3 27		//	SmartSwitch_3 Actuator	(PA5)

/*--------  ENCODER  --------*/
#define TACH_SS 53 // LS7366 Chip Select	(PB0)
#define TACH_EN 16 // LS7366 Count Enable	(PH1)

/*--------  FANS  --------*/
#define FAN1_TACH A6 //    (PF6)
#define FAN2_TACH A5 //    (PF7)
#define FAN1_PWM 40	 //    (PG1)
#define FAN2_PWM 39	 //    (PG2)

/*--------  MOTORS  --------*/
#define M1_INB 6				//	Feed Velocity		(PH3)
#define M1_INA 7				//	Feed Torque			(PH4)
#define M2_INB 8				//	Takeup Velocity	(PH5)
#define M2_INA 9				//	Takeup Torque		(PH6)
#define M1_EN A0				//	Feed Enable			(PF0)
#define M1_HLFB A1			//	Feed Output			(PF1)
#define M2_EN A2				//	Takeup Enable		(PF2)
#define M2_HLFB A3			//	Takeup Output		(PF3)
#define MOTOR_DETECT 35 //	Motor 48V Sense	(PC2)

/*--------  MAX7219  --------*/
#define MAX7219_LOAD 11 // PB5
#define MAX7219_DATA 12 // PB6
#define MAX7219_SCLK 13 // PB7

/*--------  CAMERA  --------*/
#define CAM_TRIG 19 // PD2
#define CAM_GPO1 18 // PD3
#define CAM_GPO2 3	// PE5

/*--------  LED  --------*/
#define LED_PWM 10 // PB4

/*--------  ESTOP  --------*/
#define ESTOP_OK 5		//ESTOP Light GRN	(PE3)
#define ESTOP_ALERT 2 //ESTOP Light RED	(PE4)

#endif
